<?php

/**
 * Commerce Heidelpay CheckOut
 */

/**
 * Menu callback: Redirect URL for Heidelpay
 *
 * @fixme Check if we need to impersonate the order owner to have full access on the order.
 */
function commerce_heidelpay_hco_redirect($order_id, $payment_redirect_key) {
  $response = $_POST;
  // Well, that shouldn't be necessary, but the response data encoding appears
  // to be incorrect
  commerce_heidelpay_response_prepare($response);
  $order = commerce_order_load($order_id);
  watchdog('heidelpay', 'Response from Heidelpay: !data', array('!data' => '<pre>' . check_plain(print_r($response, 1)) . '</pre>'));

  if ($payment_redirect_key !== $order->data['payment_redirect_key']) {
    watchdog('heidelpay', 'Order @order: Payment redirect key @key does not match expected @expected', array('@order' => $order_id, '@key' => $payment_redirect_key, '@expected' => $order->data['payment_redirect_key']), WATCHDOG_ERROR);
    return FALSE;
  }

  // Avoid notices by adding defaults.
  $response += array('IDENTIFICATION_UNIQUEID' => NULL, 'PROCESSING_STATUS_CODE' => NULL);

  $transaction_remote_id = $response['IDENTIFICATION_UNIQUEID'];
  // If we got this remote id more than once, ignore it.
  $previous_transactions = commerce_payment_transaction_load_multiple(array(), array('remote_id' => $transaction_remote_id));
  if ($previous_transactions) {
    watchdog('heidelpay', 'Order @order: Payment with remote id @id already processed, ignoring.', array('@order' => $order_id, '@id' => $transaction_remote_id), WATCHDOG_ERROR);
    return FALSE;
  }

  // Create a new payment transaction for the order.
  $transaction = commerce_payment_transaction_new('heidelpay', $order->order_id);

  $transaction->remote_id = $transaction_remote_id;
  $transaction->amount = commerce_currency_decimal_to_amount($response['PRESENTATION_AMOUNT'], $response['PRESENTATION_CURRENCY']);
  $transaction->currency_code = $response['PRESENTATION_CURRENCY'];
  $transaction->payload = $response;
  $transaction->remote_status = $response['PROCESSING_STATUS_CODE'];

  switch ($response['PROCESSING_RESULT']) {
    case 'ACK':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('The payment has completed.');

      // Save the transaction information.
      commerce_payment_transaction_save($transaction);
      // Proceed order to completed state.
      // @fixme This may be doubled by the return url
      /** @see commerce_payment_redirect_pane_checkout_form() */
      commerce_payment_redirect_pane_next_page($order, 'Heidelpay: Payment successful.');
      watchdog('heidelpay', 'Payment processed for Order @order_number with ID @txn_id.', array('@txn_id' => $transaction->remote_id, '@order_number' => $order->order_number), WATCHDOG_INFO);

      $url = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
      break;
    default:
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t("The payment has failed.");

      // Save the transaction information.
      commerce_payment_transaction_save($transaction);
      // Move order state back..
      // @fixme This may be doubled by the return url
      /** @see commerce_payment_redirect_pane_checkout_form() */
      commerce_payment_redirect_pane_previous_page($order, 'Heidelpay: Payment failed.');
      watchdog('heidelpay', 'Payment failed for Order @order_number with ID @txn_id.', array('@txn_id' => $transaction->remote_id, '@order_number' => $order->order_number), WATCHDOG_INFO);

      $url = url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
      break;
  }

  // Tell heidelpay where to redirect to.
  print $url;
  // Prevent any other output.
  exit();
}
